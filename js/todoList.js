export class ToDoList {
    constructor() {
        this.tdList = []
    }

    addToDo(todo) {
        this.tdList.push(todo)
    }

    printToDo() {
        let str = ''
        str = this.tdList.reduceRight((content, td, index) => {
            content += `
                <li>
                    <span>${td.content}</span>
                    <div class='buttons'>
                        <button class='remove' onclick='deleteToDo(${index},"${td.status}")' id='${index}' >
                            <i class="fa fa-trash-alt"></i>
                        </button>
                        <button class='complete' onclick='checkCompleteToDo(${index},"${td.status}")'>
                            <i class="far fa-check-circle"></i>
                            <i class="fas fa-check-circle"></i>
                    </button>
                    </div>
                </li>
            `
            return content
        }, '')
        return str
    }

    deleteToDo(index){
        console.log(index)
        this.tdList.splice(index,1)
    }

    sortToDo(des){
        this.tdList.sort((todo,next)=>{
            return (next.content.toLowerCase()).localeCompare(todo.content.toLowerCase())
        })
        if(des){
            this.tdList.reverse()
        }
    }
}