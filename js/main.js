const todoData = [
    {
        content: 'Học react',
        status: false
    },
    {
        content: 'Học java',
        status: true
    },
    {
        content: 'Học python',
        status: false
    },
]

import { ToDo } from "./todo.js"
import { ToDoList } from "./todoList.js"

const getEle = id => document.getElementById(id)
let todoList = new ToDoList(),
    completeList = new ToDoList()

const addToDo = () => {
    let str = getEle('newTask').value,
        ulTD = getEle('todo')

    if (str.trim() != '') {
        let td = new ToDo(str, 'todo');
        todoList.addToDo(td)
    } else {
        alert('Chưa nhập công việc vào (* ￣︿￣)')
        getEle('newTask').focus()
        return
    }
    printToDoList(ulTD);
    getEle('newTask').value = ''
}


const printToDoList = (ulTd) => {
    ulTd.innerHTML = todoList.printToDo();
}

const printCompleteToDoList = (ulC) => {
    ulC.innerHTML = completeList.printToDo();
}

window.addToDo = addToDo

const deleteToDo = (index, status) => {

    let ulTD = getEle('todo'),
        ulC = getEle('completed')

    if (status == 'todo') {
        todoList.deleteToDo(index)
        printToDoList(ulTD)
    }else if(status == 'completed'){
        completeList.deleteToDo(index)
        printCompleteToDoList(ulC)
    }else{
        alert('Không thể xóa công việc đó được (►__◄)')
    }
    console.log('a')
}

window.deleteToDo = deleteToDo


const checkCompleteToDo = (index,status) =>{

    let ulTD = getEle('todo'),
        ulC = getEle('completed')
    
    if(status == 'todo'){
        let td = new ToDo(todoList.tdList[index].content,'completed')
        completeList.addToDo(td)
        todoList.deleteToDo(index)
        printToDoList(ulTD)
        printCompleteToDoList(ulC)
    }else if(status == 'completed'){
        let td = new ToDo(todoList.tdList[index].content,'todo')
        todoList.addToDo(td)
        completeList.deleteToDo(index)
        printToDoList(ulTD)
        printCompleteToDoList(ulC)
    }else{
        alert('')
    }
}

window.checkCompleteToDo = checkCompleteToDo

const sortToDo = (des) =>{
    let ulTD = getEle('todo'),
    ulC = getEle('completed')
    todoList.sortToDo(des)
    completeList.sortToDo(des)
    printToDoList(ulTD)
    printCompleteToDoList(ulC)
}

window.sortToDo = sortToDo